#ifndef XMLPROC_H
#define XMLPROC_H
#include "graph.h"
#include <libxml/tree.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MAX_WEIGHT_ENCODING 100 /*It was told us its ok to assume the weight of each vertex will require less then 100 digits */

void createXMLfiles(char *filename, char *endpath);
void initColors();

char *clusterColors[10];

#endif

