#ifndef BFS_H
#define BFS_H
#include <stdio.h>
#include <stdlib.h>
#include "graph.h"

struct element
{
	int data;
	struct element *next;
	struct element *prev;
};
typedef struct element element;

struct queue
{
	int count;
	element *front;
	element *back;
};
typedef struct queue queue;

void initialize(queue *que);
void push(int data , queue *que);
int pop(queue *que);
void freeQueue(queue *que);

void bfsGraph(int sourceID , int *results);

#endif
