#include "xmlproc.h"

void createXMLfiles(char *filename, char *endpath)
{
	xmlDocPtr pXMLDom;
	xmlNodePtr pRoot;
	xmlNodePtr pNode;
	xmlNodePtr pEdge;
	xmlNodePtr pGraphics;
	int i , j;
	char vertexName[MAX_NAME];
	char edgeName[2*MAX_NAME];
	char *ID = (char*)malloc(numberOfVertices*sizeof(char));
	char weight[MAX_WEIGHT_ENCODING];
	int indicator = 0;
	char dummypath[255];
	strcpy(dummypath, endpath);

	if(strcmp(filename , "best_clusters") == 0)
		indicator = 1;
	/* create a new XML Document */
	pXMLDom = xmlNewDoc(BAD_CAST "1.0");
	/* create root element and set its properties */
	pRoot = xmlNewNode(NULL , BAD_CAST "graph");
	xmlNewProp(pRoot , BAD_CAST "label" , BAD_CAST filename);
	xmlNewProp(pRoot , BAD_CAST "directed" , BAD_CAST "0");
	xmlNewProp(pRoot , BAD_CAST "xmlns:cy" , BAD_CAST "http://www.cytoscape.org");
	xmlNewProp(pRoot , BAD_CAST "xmlns:dc" , BAD_CAST "http://purl.org/dc/elements/1.1/");
	xmlNewProp(pRoot , BAD_CAST "xmlns:rdf" , BAD_CAST "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
	xmlNewProp(pRoot , BAD_CAST "xmlns:xlink" , BAD_CAST "http://www.w3.org/1999/xlink");
	xmlNewProp(pRoot , BAD_CAST "xmlns" , BAD_CAST "http://www.cs.rpi.edu/XGMML");
	xmlDocSetRootElement(pXMLDom , pRoot);
	/* create node elements */
	for(i = 0; i < numberOfVertices; i++)
	{
		if((indicator == 1) && (vertexList[i].clusterID > 5))
			continue;
		pNode = xmlNewChild(pRoot , NULL , BAD_CAST "node" , NULL);
		xmlNewProp(pNode , BAD_CAST "label" , BAD_CAST vertexList[i].name);
		sprintf(vertexName , "%d" , vertexList[i].id);
		xmlNewProp(pNode , BAD_CAST "id" , BAD_CAST vertexName);
		pGraphics = xmlNewChild(pNode , NULL , BAD_CAST "graphics" , NULL);
		xmlNewProp(pGraphics , BAD_CAST "type" , BAD_CAST "ELLIPSE");
		if(vertexList[i].clusterID < 10)
			xmlNewProp(pGraphics , BAD_CAST "fill" , BAD_CAST clusterColors[vertexList[i].clusterID-1]);
		else
			xmlNewProp(pGraphics , BAD_CAST "fill" , BAD_CAST clusterColors[9]);
		xmlNewProp(pGraphics , BAD_CAST "width" , BAD_CAST "4");
		xmlNewProp(pGraphics , BAD_CAST "cy:nodeLabel" , BAD_CAST vertexList[i].name);
		xmlNewProp(pGraphics , BAD_CAST "cy:borderLineType" , BAD_CAST "solid");
	}
	/* create edge elements */
	for(i = 0; i < numberOfVertices-1; i++)
	{
		for(j = i+1; j < numberOfVertices; j++)
		{
			
			if(((indicator == 0) || ((indicator == 1) && (vertexList[i].clusterID <=5 && vertexList[j].clusterID <=5))) && (edgeMatrix[i][j].type == 'R'))
			{
				pEdge = xmlNewChild(pRoot , NULL , BAD_CAST "edge" , NULL);
				sprintf(edgeName , "%s (interaction detected) %s" , vertexList[i].name , vertexList[j].name);
				xmlNewProp(pEdge , BAD_CAST "label" , BAD_CAST edgeName);
				sprintf(ID , "%d" , i);
				xmlNewProp(pEdge , BAD_CAST "source" , BAD_CAST ID);
				sprintf(ID , "%d" , j);
				xmlNewProp(pEdge , BAD_CAST "target" , BAD_CAST ID);	
				pGraphics = xmlNewChild(pEdge , NULL , BAD_CAST "graphics" , NULL);
				sprintf(weight , "weight=%.3f" , edgeMatrix[i][j].weight);
				xmlNewProp(pGraphics , BAD_CAST "cy:edgeLabel" , BAD_CAST weight);			
			}
		}
	}
	/* Save xml file */
	if(indicator == 0){
		strcat(dummypath, "clustering_solution.xgmml");
		xmlSaveFileEnc(dummypath , pXMLDom , "UTF-8");
	}
	else{
		strcat(dummypath, "best_clusters.xgmml");
		xmlSaveFileEnc(dummypath , pXMLDom , "UTF-8");
	}
	/* free XML environment */
	free(ID);
	xmlFreeDoc(pXMLDom);
	xmlCleanupParser();
	xmlMemoryDump();
}

void initColors()
{
	clusterColors[0] = "#0000FF";	
	clusterColors[1] = "#00FFFF";
	clusterColors[2] = "#8A2BE2";
	clusterColors[3] = "#A52A2A";
	clusterColors[4] = "#7FFF00";
	clusterColors[5] = "#006400";
	clusterColors[6] = "#FFD700";
	clusterColors[7] = "#FF69B4";
	clusterColors[8] = "#FF4500";
	clusterColors[9] = "#C0C0C0";
}
