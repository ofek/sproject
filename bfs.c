#include "bfs.h"

void initialize(queue *que)
{
	que->count = 0;
	que->front = NULL;
	que->back = NULL;
}

void push(int data , queue *que)
{
	element *elem = NULL;
	elem = (element*)malloc(sizeof(element));
	elem->data = data;
	elem->next = que->back;
	elem->prev = NULL;
	if(que->count == 0) /* this is the first element */
		que->front = elem;
	else
		que->back->prev = elem;
	que->back = elem;
	que->count = que->count + 1;
}

int pop(queue *que)
{
	int data;
	element *elem;
	if(que->count == 0)
	{
		printf("Error : queue is empty! \n");
		return 1;
	}
	elem = que->front;
	data = elem->data;
	que->front = elem->prev;
	free(elem);
	que->count = que->count - 1;
	return data;	
}

void freeQueue(queue *que)
{
	while(que->count != 0)
		printf("%d\n" , pop(que));
	free(que);
}

void bfsGraph(int sourceID , int *results)
{
	queue *que = (queue*)malloc(sizeof(queue));
	int maxDist = numberOfVertices;
	vertex currentNode;
	int currentNeighbor;
	int i;
	for(i = 0 ; i < numberOfVertices;i++)
		results[i] = maxDist;
	results[sourceID] = 0;
	initialize(que);
	push(sourceID , que);
	while(que->count != 0)
	{
		currentNode = vertexList[pop(que)];
		for(i = 0; i < currentNode.numberOfIncomingEdges; i++)
		{
			currentNeighbor = currentNode.incomingEdges[i];
			if(results[currentNeighbor] == maxDist)
			{
				results[currentNeighbor] = results[currentNode.id] + 1;
				push(currentNeighbor , que);
			}		
		}
	}
	freeQueue(que);		
}

