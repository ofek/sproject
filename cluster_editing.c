/*------------------------------------------------------------------------*/
/*  File: cluster.c                                                       */
/*  Version 1.0                                                           */
/*------------------------------------------------------------------------*/
/*  Written for Software Project Class.                                   */
/*  Based uopn qpex1.c                                                    */
/*  Permission is expressly granted to use this example in the            */
/*  course of developing applications that use ILOG products.             */
/*------------------------------------------------------------------------*/

/* cluster.c - Entering and optimizing k-clustering integer linear programming problem */
/* You may and should alter the file */

#include "cluster_editing.h"
#define IS_VALUE_1(X) ((1-X) < 0.00001)

/* This routine initializes the cplex enviorement, sets screen as an output for cplex errors and notifications, 
   and sets parameters for cplex. It calls for a mixed integer program solution and frees the environment.
   To Do:
   Declare the parameters for the problem and fill them accordingly. After creating the program thus, copy it into cplex. 
   Define any integer or binary variables as needed, and change their type before the call to CPXmipopt to solve problem. 
   Use CPXwriteprob to output the problem in lp format, in the name of <k>.lp.
   Read solution (both objective function value and variables assignment). 
   Communicate to pass the problem and the solution between the modules in the best way you see. 
*/

/* This function calculates score and diameter for all clusters */

void calcClusterScoreAndDiameter()
{
	int *results = (int*)malloc(numberOfVertices*sizeof(int));
	double clusterScore;
	int diameter;
	int maxDist;
	int i,j,k;
	for(i = 0; i < numberOfClusters; i++)
	{
		clusterList[i].ID = -1;
		/* calculate clusters score */
		clusterScore = 0;
		if(clusterList[i].size == 1)
		{
			clusterList[i].score = 0;
			clusterList[i].diameter = 0;
			continue;
		}
		/* the nodesID is ordered in ascending order */
		for(j = 0; j < clusterList[i].size-1; j++)		
		{
			for(k = j+1; k < clusterList[i].size; k++)
			{
				if(edgeMatrix[clusterList[i].nodesID[j]][clusterList[i].nodesID[k]].type == 'R')
					clusterScore = clusterScore + edgeMatrix[clusterList[i].nodesID[j]][clusterList[i].nodesID[k]].weight;
			}
		}
		clusterList[i].score = clusterScore;
		/* calculate diameter */
		diameter = 0;
		maxDist = 0;
		for(j = 0; j < clusterList[i].size; j++)
		{
			bfsGraph(clusterList[i].nodesID[j] , results);
			for(k = 0; k < clusterList[i].size; k++)
			{
				if(maxDist < results[clusterList[i].nodesID[k]])
					maxDist = results[clusterList[i].nodesID[k]];
			}
			if(maxDist > diameter)
				diameter = maxDist;
		}
		clusterList[i].diameter = diameter;	
	}
//	free(results);	
}

/* This function sorts the clusters and gives appropriate ID to each cluster */

void sortClusters()
{
	int maxIndex = 0;
	int clustersOrdered = 0;
	int i,j;
	for(i = 0; i < numberOfClusters; i++)
	{
		for(j = 0; j < numberOfClusters; j++)
		{
			if(clusterList[j].ID == -1) /*we havent found the j'th cluster place in the ordered cluster list yet */
			{
				if(clusterList[j].size > clusterList[maxIndex].size)
					maxIndex = j;
				else if((clusterList[j].size == clusterList[maxIndex].size) && (clusterList[j].score > clusterList[maxIndex].score))
					maxIndex = j;
			}
		}
		clusterList[maxIndex].ID = clustersOrdered+1;
		clustersOrdered++;
		/* find the first index which hasnt been ordered */
		maxIndex = 0;
		for(j = 0; j < numberOfClusters; j++)
		{
			if(clusterList[j].ID == -1)
			{
				maxIndex = j;
				break;
			}
		}
		
	}
	/* update correct clusterID in all vertices */
	for(i = 0 ; i < numberOfClusters; i++)
	{
		for(j = 0; j < clusterList[i].size; j++)
			vertexList[clusterList[i].nodesID[j]].clusterID = clusterList[i].ID;
	}
	
}

/* This function calcuates average weight in clusters and average weight between clusters */

void calcAvrWeight()
{
	double sumOfWeightsIn = 0.0;
	double sumOfWeightsBetween = 0.0;
	int numOfEdgesIn = 0;
	int numOfEdgesBetween = 0;
	int i,j;
	for(i = 0 ; i < numberOfVertices-1; i++)
	{
		for(j = i+1; j < numberOfVertices; j++)
		{
			if((edgeMatrix[i][j].type == 'R') && (vertexList[i].clusterID == vertexList[j].clusterID))
			{
				numOfEdgesIn++;
				sumOfWeightsIn = sumOfWeightsIn + edgeMatrix[i][j].weight;
			}
			if((edgeMatrix[i][j].type == 'R') && (vertexList[i].clusterID != vertexList[j].clusterID))
			{
				numOfEdgesBetween++;
				sumOfWeightsBetween = sumOfWeightsBetween + edgeMatrix[i][j].weight;
			}
		}
	}
	if(numOfEdgesIn != 0)
		avrWeightInClusters = sumOfWeightsIn / numOfEdgesIn;
	if(numOfEdgesBetween != 0)
		avrWeightBetweenClusters = sumOfWeightsBetween / numOfEdgesBetween;		
}

/* this function calculates the required properties of each cluster. it assumes that the nodesID list in each cluster object have already been initialized */

void calculateClusterStatistics()
{
	calcClusterScoreAndDiameter();
	sortClusters();
	calcAvrWeight();
}

/* this function calculates the minimal cost of modifications */

void calculateScore(double *solution , int **varsOrder)
{
	double sumOfRealEdges = 0;
	double sumOfWeightsSolution = 0;
	int i,j;
	if(numberOfVertices == 1)
	{
		score = 0;
		return;
	}
	for(i = 0; i < numberOfVertices-1; i++)
	{
		for(j = i+1; j < numberOfVertices; j++)
		{
			if(edgeMatrix[i][j].type == 'R')
				sumOfRealEdges = sumOfRealEdges + edgeMatrix[i][j].weight;
			if(IS_VALUE_1(solution[varsOrder[i][j]]))
				sumOfWeightsSolution = sumOfWeightsSolution + edgeMatrix[i][j].weight;
		}
	}
	score = sumOfRealEdges - sumOfWeightsSolution;
}

/* This function finds the cluster of every node */
void extractInfoFromSolution(double *solution , int **varsOrder)
{
	int numOfClusters = 0;
	int *clustersRep = (int*)malloc(numberOfVertices*sizeof(int)); /* represent each cluster by an id of a node it contains */
	int isFound = 0;
	int verID;
	int numberOfElementsInCluster;
	int i , j , k;
	for(i = 0; i < numberOfVertices-1; i++)
	{
		for(j = i+1; j < numberOfVertices; j++)
		{
			if(IS_VALUE_1(solution[varsOrder[i][j]])) /* X_ij appears in the solution */
			{
				/* the solution is a grpah of cliques. find all vertices that appears in the solution connected to X_ij */
				/*first check if we already found the clique containing X_ij */
				isFound = 0;
				verID = edgeMatrix[i][j].id1; /* verID is an ID for a node that connected to this edge */
				/* if there is an edge in the solution that connects vertex verID to a vertex in clustersRep */
				/* then we have already found the cluster containing the current edge */
				for(k = 0; k < numOfClusters; k++)
				{
					if(verID > clustersRep[k])
					{
						if(IS_VALUE_1(solution[varsOrder[clustersRep[k]][verID]]))
						{
							isFound = 1;
							break;
						}
					}
					if(verID < clustersRep[k])
					{
						if(IS_VALUE_1(solution[varsOrder[verID][clustersRep[k]]]))
						{
							isFound = 1;
							break;
						}		
					}
					if(verID == clustersRep[k])
					{
						isFound = 1;
						break;
					}	
				}
				/* if edge not found - we found a new cluster */
				if(isFound == 0)
				{
					vertexList[verID].clusterID = numOfClusters;
					/* find all vertices in the new cluster and update cluster field accordingly */
					for(k = 0; k < numberOfVertices; k++)
					{
						if(verID > k)
						{
							if(IS_VALUE_1(solution[varsOrder[k][verID]]))
								vertexList[k].clusterID = numOfClusters;
						}
						if(verID < k)
						{
							if(IS_VALUE_1(solution[varsOrder[verID][k]]))
								vertexList[k].clusterID = numOfClusters;
						}
					}
					clustersRep[numOfClusters] = verID;
					numOfClusters++;
				}				
			}
		}
	}
	/* check for additional 1-node clusters */
	for(i = 0; i < numberOfVertices; i++)
	{
		if(vertexList[i].clusterID == -1)
		{
			vertexList[i].clusterID = numOfClusters;
			numOfClusters++;
		}
	}
	numberOfClusters = numOfClusters;
//	free(clustersRep);
}

/*This function initializes the global cluster list */

void initializeClusterList()
{
	int numberOfElementsInCluster;
	int i,j;
	clusterList = (graphCluster*)malloc(numberOfClusters*sizeof(graphCluster));
	for(i = 0; i < numberOfClusters; i++)
	{
		/* calculate number of nodes appear in cluster i */
		numberOfElementsInCluster = 0;
		for(j = 0; j < numberOfVertices; j++)
		{
			if(vertexList[j].clusterID == i)
				numberOfElementsInCluster++;
		}
		clusterList[i].nodesID = (int*)malloc(numberOfElementsInCluster*sizeof(int));
		clusterList[i].size = numberOfElementsInCluster;
		/* get nodes IDs */
		numberOfElementsInCluster = 0;
		for(j = 0; j < numberOfVertices; j++)
		{
			if(vertexList[j].clusterID == i)
			{
				clusterList[i].nodesID[numberOfElementsInCluster] = j;
				numberOfElementsInCluster++;
			}
		}
	}	
}

/* this function updates the cluster field of the nodes in the graph according to the solution */
 
void createClusters(double *solution , int **varsOrder)
{
	extractInfoFromSolution(solution , varsOrder);
	initializeClusterList();
	calculateClusterStatistics();
	calculateScore(solution , varsOrder);	
}

int cluster(double cost, char *endpath)
{
	int numcols = numberOfVertices*(numberOfVertices-1)/2; /* number of variables is (|V| choose 2) */
	int numrows = numberOfVertices*(numberOfVertices-1)*(numberOfVertices-2)/2; /* number of constrains is 3*(|V| choose 3)	*/
	int **varsOrder = (int**)malloc(numberOfVertices*sizeof(int*)); /* order the variables */
	varConstraints *constraintsList = (varConstraints*)malloc(numcols*sizeof(varConstraints)); /* contains constraints for each var */
	/* parameters for CPXcolylp */
	double *obj;
	int objsen = CPX_MAX;
	double *rhs;
	char *sense;
	int *matbeg;
	int *matcnt;
	int *matind;
	double *matval;
	double *lb;
	double *ub;
	/* parameters for CPXchgctype */
	int *indices = (int*)malloc(numcols*sizeof(int));
	char *ctype = (char*)malloc(numcols*sizeof(char));
	/* environment stuff */
	char probname[255];
	CPXENVptr p_env = NULL;
	CPXLPptr  p_lp = NULL;
	/* solution statistics */
	double funcVal;
	int solstatus;
	double *solution = (double*)malloc(numcols*sizeof(int));
	int status;  
	int count = 0;
	int i , j , k;
	FILE *result;
	char dummypath[255];
	strcpy(dummypath, endpath);	
	strcpy(probname, endpath);	
	strcat(probname, "clustering_solution.lp");
	
	/* Initialize the CPLEX environment */
	p_env = CPXopenCPLEX (&status);

	/* If an error occurs, the status value indicates the reason for
	failure.  A call to CPXgeterrorstring will produce the text of
	the error message. Note that CPXopenCPLEX produces no output,
	so the only way to see the cause of the error is to use
	CPXgeterrorstring. For other CPLEX routines, the errors will
	be seen if the CPX_PARAM_SCRIND indicator is set to CPX_ON.  */

	if ( p_env == NULL ) 
	{
		char  errmsg[1024];
	    	fprintf (stderr, "Error: Could not open CPLEX environment.\n");
	        CPXgeterrorstring (p_env, status, errmsg);
	        fprintf (stderr, "%s", errmsg);
	        goto TERMINATE;
	}

	/* Turn on output to the screen */
	status = CPXsetintparam (p_env, CPX_PARAM_SCRIND, CPX_ON);
	if ( status ) 
	{
		fprintf (stderr, "Error: Failure to turn on screen indicator, error %d.\n", status);
	        goto TERMINATE;
	}
	for(i = 0; i < numberOfVertices; i++)
		varsOrder[i] = (int*)malloc(numberOfVertices*sizeof(int));
	obj = (double*)malloc(numcols*sizeof(double));
	lb = (double*)malloc(numcols*sizeof(double));
	ub = (double*)malloc(numcols*sizeof(double));
	/* initialize obj array. we maximize the function s(i,j)*X_ij for each edge e = (i,j) */
	/* initialize varsOrder and allocate place for varConstraints */
	/* initialize lower bound and upper bound for all variables */
	/* initialize indices and ctype for CPXchgctype */
	for(i = 0; i < numberOfVertices-1; i++)
	{
		for(j = i+1; j < numberOfVertices; j++)
		{
			obj[count] = edgeMatrix[i][j].weight;
			lb[count] = 0;
			ub[count] = 1;
			/* each variable appears in (numberOfVertices-2)*3 constraints */
			constraintsList[count].constraintsID = (int*)malloc((numberOfVertices-2)*3*sizeof(int));
			constraintsList[count].constraintsCoef = (int*)malloc((numberOfVertices-2)*3*sizeof(int)); 
			constraintsList[count].numOfConstraints = 0;
			varsOrder[i][j] = count;
			/* init inidices and ctype */
			indices[count] = count;
			ctype[count] = 'I';
			count++;
		}
	}
	/* initialize right hand side and sense in all constrains */
	/* initialize constraintsList - get for each variable the list of constraints it appears in and the list of coefficents */
	count = 0; /* count will represent here the constraint id */
	rhs = (double*)malloc(numrows*sizeof(double));
	sense = (char*)malloc(numrows*sizeof(char));
	for(i = 0; i < numberOfVertices-2;i++)
	{
		for(j = i+1; j < numberOfVertices-1;j++)
		{
			for(k = j+1; k < numberOfVertices;k++)
			{
				/* for each i < j < k we have 3 constraints */
				/* first constraint : X_ij + X_jk - X_ik <= 1 */
				rhs[count] = 1;
				sense[count] = 'L';
				constraintsList[varsOrder[i][j]].constraintsID[constraintsList[varsOrder[i][j]].numOfConstraints] = count;
				constraintsList[varsOrder[i][j]].constraintsCoef[constraintsList[varsOrder[i][j]].numOfConstraints] = 1;
				constraintsList[varsOrder[i][j]].numOfConstraints++;
				constraintsList[varsOrder[j][k]].constraintsID[constraintsList[varsOrder[j][k]].numOfConstraints] = count;
				constraintsList[varsOrder[j][k]].constraintsCoef[constraintsList[varsOrder[j][k]].numOfConstraints] = 1;
				constraintsList[varsOrder[j][k]].numOfConstraints++;
				constraintsList[varsOrder[i][k]].constraintsID[constraintsList[varsOrder[i][k]].numOfConstraints] = count;
				constraintsList[varsOrder[i][k]].constraintsCoef[constraintsList[varsOrder[i][k]].numOfConstraints] = -1;
				constraintsList[varsOrder[i][k]].numOfConstraints++;
				count++;
				/* second constraint : X_ij - X_jk + X_ik <= 1 */
				rhs[count] = 1;
				sense[count] = 'L'; 
				constraintsList[varsOrder[i][j]].constraintsID[constraintsList[varsOrder[i][j]].numOfConstraints] = count;
				constraintsList[varsOrder[i][j]].constraintsCoef[constraintsList[varsOrder[i][j]].numOfConstraints] = 1;
				constraintsList[varsOrder[i][j]].numOfConstraints++;
				constraintsList[varsOrder[j][k]].constraintsID[constraintsList[varsOrder[j][k]].numOfConstraints] = count;
				constraintsList[varsOrder[j][k]].constraintsCoef[constraintsList[varsOrder[j][k]].numOfConstraints] = -1;
				constraintsList[varsOrder[j][k]].numOfConstraints++;
				constraintsList[varsOrder[i][k]].constraintsID[constraintsList[varsOrder[i][k]].numOfConstraints] = count;
				constraintsList[varsOrder[i][k]].constraintsCoef[constraintsList[varsOrder[i][k]].numOfConstraints] = 1;
				constraintsList[varsOrder[i][k]].numOfConstraints++;
				count++;
				/* third constraint : -X_ij + X_jk + X_ik <=1 */
				rhs[count] = 1;
				sense[count] = 'L';
				constraintsList[varsOrder[i][j]].constraintsID[constraintsList[varsOrder[i][j]].numOfConstraints] = count;
				constraintsList[varsOrder[i][j]].constraintsCoef[constraintsList[varsOrder[i][j]].numOfConstraints] = -1;
				constraintsList[varsOrder[i][j]].numOfConstraints++;
				constraintsList[varsOrder[j][k]].constraintsID[constraintsList[varsOrder[j][k]].numOfConstraints] = count;
				constraintsList[varsOrder[j][k]].constraintsCoef[constraintsList[varsOrder[j][k]].numOfConstraints] = 1;
				constraintsList[varsOrder[j][k]].numOfConstraints++;
				constraintsList[varsOrder[i][k]].constraintsID[constraintsList[varsOrder[i][k]].numOfConstraints] = count;
				constraintsList[varsOrder[i][k]].constraintsCoef[constraintsList[varsOrder[i][k]].numOfConstraints] = 1;
				constraintsList[varsOrder[i][k]].numOfConstraints++;
				count++;				
			}
		}
	}
	/* initialize matbeg, matcnd , matind and matval */
	count = 0;
	matbeg = (int*)malloc(numcols*sizeof(int));
	matcnt = (int*)malloc(numcols*sizeof(int));
	matval = (double*)malloc(numcols*3*(numberOfVertices-2)*sizeof(double));
	matind = (int*)malloc(numcols*3*(numberOfVertices-2)*sizeof(int));
	for(i = 0; i < numberOfVertices-1;i++)
	{
		for(j = i+1; j < numberOfVertices; j++)
		{
			matcnt[varsOrder[i][j]] = 3*(numberOfVertices-2); /* the number of the constraints the variable appears in */
			matbeg[varsOrder[i][j]] = varsOrder[i][j]*3*(numberOfVertices-2); /* the index in matind and matval where the data of the the current variable starts */
			for(k = 0; k < constraintsList[varsOrder[i][j]].numOfConstraints; k++) /* numOfConstraints should be 3*(numVert-2) */
			{
				matind[count] = constraintsList[varsOrder[i][j]].constraintsID[k];
				matval[count] = constraintsList[varsOrder[i][j]].constraintsCoef[k];
				count++;
			}
		}
	}
	/* Create the problem. */
	p_lp = CPXcreateprob (p_env, &status, probname);

	/* A returned pointer of NULL may mean that not enough memory
	was available or there was some other problem.  In the case of 
	failure, an error message will have been written to the error 
	channel from inside CPLEX. The setting of
	the parameter CPX_PARAM_SCRIND causes the error message to
	appear on stdout.  */

	if ( p_lp == NULL ) 
	{
		fprintf (stderr, "Error: Failed to create problem.\n");
	        goto TERMINATE;
	}

	/* Use CPXcopylp to transfer the ILP part of the problem data into the cplex pointer lp */   
	status = CPXcopylp(p_env , p_lp , numcols , numrows , objsen , obj , rhs , sense , matbeg , matcnt , matind , matval , lb , ub , NULL);
	if(status)
	{
		fprintf(stderr , "Error : Failed to copy the problem into memroy.\n");
		goto TERMINATE;
	}
	/* Change variables types to integers */
	status = CPXchgctype(p_env , p_lp , numcols , indices , ctype);
	if(status)
	{
		fprintf(stderr , "Error : function CPXchgctype failed.\n");
		goto TERMINATE;
	} 
	/* Optimize the problem. */
	status = CPXmipopt (p_env, p_lp);
	if ( status ) 
	{
		fprintf (stderr, "Error: Failed to optimize problem.\n");
	        goto TERMINATE;
	}
	/* Write a copy of the problem to a file. 
	Please put into probname the following string: Output Directory + "clustering_solution.lp" to create clustering_solution.lp in your 		output directory */
	status = CPXwriteprob (p_env, p_lp, probname, NULL);
	if ( status ) 
	{
		fprintf (stderr, "Error: Failed to write LP to disk.\n");
	        goto TERMINATE;
	}
	/* Obtain solution */
	status = CPXsolution(p_env , p_lp , &solstatus , &funcVal , solution , NULL , NULL , NULL);
	if ( status ) 
	{
      		fprintf (stderr, "Failed to obtain solution.\n");
      		goto TERMINATE;
   	}
	/* create clusters */
	createClusters(solution , varsOrder);
	if(numberOfVertices == 0){
		printf("Error: There are no vertices!\n");
		return 1;
	}
	strcat(dummypath, "results");
   	result = fopen(dummypath , "w");
	printData(result , cost);
	fclose(result);
	/* create xml files */
	createXMLfiles("clustering_solution", endpath);
	createXMLfiles("best_clusters", endpath);
	TERMINATE:
	     
	return (status);
}  


/* This simple routine frees up the pointer *ptr, and sets *ptr to NULL */
void free_and_null (char **ptr)
{
   if ( *ptr != NULL ) {
	free (*ptr);
	*ptr = NULL;
   }
}

/* init global variables */
vertex *vertexList = NULL;
edge **edgeMatrix = NULL;
int numberOfVertices = 0;
int maxNumberOfVertices = 1;
int numberOfEdges = 0;
graphCluster *clusterList = NULL;
int numberOfClusters = 0;
double score = 0;
double avrWeightInClusters = 0;
double avrWeightBetweenClusters = 0;


int main(int argc , char *argv[])
{

	FILE *input;
	FILE *result;
	initColors();

	double cost;
	char* endp;
	char dummypath1[255];
	char dummypath2[255];

	strcpy(dummypath1, argv[1]);
	strcpy(dummypath2, argv[2]);
	strcat(dummypath1, "network");
	strcat(dummypath2, "results");	

	if (argc != 4){
		printf("Error: Program call should include exactly 4 parameters (including executable name)!\n");
		return 1;
	}
	else{
		cost = strtod(argv[3], &endp);
	}

	if (cost < 0 || cost > 1 || *endp != 0){
		printf("Error: C value must be between 0 and 1, inclusive\n");
		return 1;
	}
	input = fopen(dummypath1 , "r");
        result = fopen(dummypath2 , "w");
	strcpy(dummypath2, argv[2]);
	if (input == NULL){
		printf("Error: bad input folder\n");
		return 1;
	}
	if (result == NULL){
		printf("Error: bad output folder\n");
		return 1;
	}
	createGraph(input , cost);
	fclose(input);
	cluster(cost, dummypath2);
	printData(result , cost);
	fclose(result);	
}
