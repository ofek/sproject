#ifndef CLUSTER_EDITING_H
#define CLUSTER_EDITING_H

/* Bring in the CPLEX function declarations and the C library 
   header file stdio.h with include of cplex.h. */
#include <ilcplex/cplex.h>
#include <stdlib.h>
#include "graph.h"
#include "bfs.h"
#include "xmlproc.h"

/* Bring in the declarations for the string functions */
#include <string.h>

/* Include declaration for functions */
struct varConstraints
{
	int *constraintsID;
	int *constraintsCoef;
	int numOfConstraints;
};
typedef struct varConstraints varConstraints;
	
void free_and_null (char **ptr);
void extractInfoFromSolution(double *solution , int **varsOrder);
void initializeClusterList();
void createClusters(double *solution , int **varsOrder);
void calcClusterScoreAndDiameter();
void sortClusters();
void calcAvrWeight();
void calculateClusterStatistics();
void calculateScore(double *solution , int **varsOrder);
int  cluster(double cost, char *endpath);


#endif
