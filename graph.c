#include "graph.h"

int add_vertex(char *name)
{
	vertex newVertex;
	newVertex.name = (char*)malloc(sizeof(char)*MAX_NAME);
	if(newVertex.name == NULL)
	{
		perror("Error: standard function malloc has failed\n");
		freeMemory();
		return 1;
	}
	strcpy(newVertex.name , name);
	newVertex.id = numberOfVertices;
	newVertex.incomingEdges = (int*)malloc(sizeof(int));
	if(newVertex.incomingEdges == NULL)
	{
		perror("Error: standard function malloc has failed\n");
		freeMemory();
		return 1;
	}
	newVertex.weights = (double*)malloc(sizeof(double));
	if(newVertex.weights == NULL)
	{
		perror("Error: standard function malloc has failed\n");
		freeMemory();
		return 1;
	}
	newVertex.numberOfIncomingEdges = 0;
	newVertex.maxNumberOfIncomingEdges = 1;
	if(numberOfVertices == maxNumberOfVertices)
	{
		maxNumberOfVertices = 2*maxNumberOfVertices;
		vertexList = (vertex*)realloc(vertexList , sizeof(vertex)*maxNumberOfVertices);
		if(vertexList == NULL)
		{
			perror("Error: standard function realloc has failed\n");
			freeMemory();
			return 1;
		}
	}
	newVertex.clusterID = -1; /* the cluster of new vertex is still unknown at this point */
	vertexList[numberOfVertices] = newVertex;
	numberOfVertices = numberOfVertices + 1;
	return 0;
}

int add_directed_edge(int firstID , int secondID , double weight)
{
	/* update incoming edge */
	vertex ver = vertexList[secondID];
	if(ver.numberOfIncomingEdges == ver.maxNumberOfIncomingEdges)
	{
		ver.maxNumberOfIncomingEdges = 2*ver.maxNumberOfIncomingEdges;
		ver.incomingEdges = (int*)realloc(ver.incomingEdges , sizeof(int)*ver.maxNumberOfIncomingEdges);
		ver.weights = (double*)realloc(ver.weights , sizeof(double)*ver.maxNumberOfIncomingEdges);
		if((ver.incomingEdges == NULL) || (ver.weights == NULL))
		{
			perror("Error: standard function realloc has failed\n");
			freeMemory();
			return 1;
		}		
	}
	ver.incomingEdges[ver.numberOfIncomingEdges] = firstID;
	ver.weights[ver.numberOfIncomingEdges] = weight;
	ver.numberOfIncomingEdges = ver.numberOfIncomingEdges + 1;
	vertexList[secondID] = ver;
	return 0;
}

int add_edge(int firstID , int secondID , double weight)
{
	add_directed_edge(firstID , secondID , weight);
	add_directed_edge(secondID , firstID , weight);
	numberOfEdges = numberOfEdges + 1;
	return 0;
}

void createGraph(FILE *fp , double cost)
{
	char *word = (char*)malloc(500*sizeof(char));
	char *firstID;
	char *secondID;
	char *weight;
	int i , j , k;
	int isFound;
	double edgeCost = 0;
	edge newEdge;
	vertexList = (vertex*)malloc(sizeof(vertex));
	while (fscanf(fp , "%s" , word) == 1)
	{
		if(strcmp(word , "add_vertex") == 0)
		{
			free(word);
			word = (char*)malloc(500*sizeof(char));
			fscanf(fp , "%s" , word); /* now word contains the vertex name */
			add_vertex(word);
		}
		else if(strcmp(word , "add_edge") == 0)
		{
			firstID = (char*)malloc(500*sizeof(char));
			secondID = (char*)malloc(500*sizeof(char));
			weight = (char*)malloc(500*sizeof(char));
			fscanf(fp , "%s %s %s" , firstID , secondID , weight);
			add_edge(atoi(firstID) , atoi(secondID) , atof(weight));
			free(firstID);
			free(secondID);
			free(weight);
		}
		free(word);
		word = (char*)malloc(500*sizeof(char));
	}
	free(word);
	/* create full graph - add ghost edges */
	edgeMatrix = (edge**)malloc(numberOfVertices*sizeof(edge*));
	for(i = 0; i < numberOfVertices; i++)
		edgeMatrix[i] = (edge*)malloc(numberOfVertices*sizeof(edge));
	for(i = 0; i < numberOfVertices-1; i++)
	{
		for(j = i+1; j < numberOfVertices; j++)
		{
			newEdge.id1 = i;
			newEdge.id2 = j;
			isFound = 0;
			for(k = 0; k < vertexList[i].numberOfIncomingEdges; k++)
			{
				if(vertexList[i].incomingEdges[k] == j)
				{
					isFound = 1;
					edgeCost = vertexList[i].weights[k];
					break;
				}
			}
			if(isFound)
			{
				newEdge.weight = edgeCost;
				newEdge.type = 'R'; /*real edge */	
			}
			else
			{
				newEdge.weight = -cost;
				newEdge.type = 'G'; /*ghost edge */
			}
			edgeMatrix[i][j] = newEdge;
			edgeMatrix[j][i] = newEdge;
		}
	}
}

void printData(FILE *out , double cost)
{
	int i,j;
	int currentIndex;
	int currentID;
	fprintf(out , "Cluster Editing Optimal Solution\n");
	fprintf(out , "score: %.3f\nnumber of clusters: %d\ncost for ghost: %.3f\n\n" , score , numberOfClusters , cost);
	fprintf(out , "The clustered network:\n");
	/* print vertices */
	fprintf(out , "%d vertices:\n" , numberOfVertices);
	for(i = 0; i < numberOfVertices; i++)
		fprintf(out , "%s %d\n" , vertexList[i].name , vertexList[i].clusterID);
	/* print edges */
	if (numberOfEdges > 0){
		fprintf(out , "%d edges:\n" , numberOfEdges);
		for(i = 0; i < numberOfVertices;i++)
		{
			for(j = 0; j < vertexList[i].numberOfIncomingEdges; j++)
			{
				currentIndex = vertexList[i].incomingEdges[j];
				if(i < currentIndex)
					fprintf(out , "%s-%s %.3f\n" , vertexList[i].name , vertexList[currentIndex].name , vertexList[i].weights[j]);
			}
		}
	}
	fprintf(out , "\nClustering statistics for the %d clusters:\n" , numberOfClusters);
	fprintf(out , "Average weight of an edge within clusters: %.3f\n" , avrWeightInClusters);
	fprintf(out , "Average weight of an edge between clusters: %.3f\n" , avrWeightBetweenClusters);
	/* print clusters */
	currentID = 1;
	while(currentID <= numberOfClusters)
	{
		for(i = 0; i < numberOfClusters; i++)
		{
			if(clusterList[i].ID == currentID)
			{
				fprintf(out , "Cluster %d score: %.3f diameter: %d\n" , currentID , clusterList[i].score , clusterList[i].diameter);
				currentID++;
				break;
			}
		}
	}
}

void freeMemory()
{
	/* free all edges */
	int i;
	for(i = 0; i < numberOfVertices; i++)
		free(edgeMatrix[i]);
	free(edgeMatrix);
	/* free all vertices */
	for(i = 0; i < numberOfVertices; i++)
	{
		free(vertexList[i].name);
		free(vertexList[i].incomingEdges);
		free(vertexList[i].weights);
	}
	free(vertexList);
	/* free clusters */
	for(i = 0; i < numberOfClusters; i++)
		free(clusterList[i].nodesID);
	free(clusterList);
}

