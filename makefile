CPLEX=cplex125
CPARCH=x86-64_sles10_4.1

CFLAGS+= -ansi -pedantic-errors -Wall -g
CFLAGS+= -I/usr/local/stow/${CPLEX}/lib/${CPLEX}/include
CFLAGS+= -I/usr/include/libxml2
CFLAGS+= -std=c99

LDFLAGS+= -L/usr/local/stow/${CPLEX}/lib/${CPLEX}/lib/${CPARCH}/static_pic -lilocplex -lcplex 
LDFLAGS+= -lm -lpthread -lgraph
LDFLAGS+= -lxml2

all: Cluster

clean:
	rm graph.o bfs.o cluster_editing.o xmlproc.o Cluster

Cluster: cluster_editing.o bfs.o graph.o xmlproc.o
	gcc -o Cluster cluster_editing.o graph.o bfs.o xmlproc.o $(CFLAGS) $(LDFLAGS)

cluster_editing.o: cluster_editing.h cluster_editing.c
	gcc -c cluster_editing.c $(CFLAGS)

xmlproc.o: xmlproc.h xmlproc.c
	gcc -c xmlproc.c $(CFLAGS)

bfs.o: bfs.h bfs.c
	gcc -c bfs.c $(CFLAGS)

graph.o: graph.h graph.c
	gcc -c graph.c $(CFLAGS)

