#ifndef GRAPH_H
#define GRAPH_H
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MAX_NAME 100 /* It was told us that its ok to assume that the len(name) < 1000 for every vertex in the graph */

struct vertex
{
	char *name;
	int id;
	int *incomingEdges;
	double *weights;
	int numberOfIncomingEdges;
	int maxNumberOfIncomingEdges;
	int clusterID;
};
typedef struct vertex vertex;

struct edge
{
	int id1;
	int id2;
	double weight;
	char type;
};
typedef struct edge edge;

struct cluster
{
	int* nodesID;
	int size;
	double score;
	int diameter;
	int ID;	
};
typedef struct cluster graphCluster;

/* global variables */
vertex *vertexList;
edge **edgeMatrix;
int numberOfVertices;
int maxNumberOfVertices;
int numberOfEdges;
graphCluster *clusterList;
int numberOfClusters;
double score;
double avrWeightInClusters;
double avrWeightBetweenClusters;

int add_vertex(char *name);
int add_directed_edge(int firstID , int secondID , double weight);
int add_edge(int firstID , int secondID , double weight);
void createGraph(FILE *fp , double cost);
void printData(FILE *out , double cost);
void freeMemory();

#endif
